import Markup from '../Types/Markup.js'

export default class TextBuilder {

    constructor() {
        this.init();
    }

    init() {
        this.markup = new Markup();
    }

    withParagraph(content){
        let currentContent = this.markup.content;
        this.markup.content = currentContent + content;
    }

    build() {
        return this.markup;
    }
}
