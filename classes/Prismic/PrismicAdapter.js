import Logger from "../Logger.js";
import ComponentType from "../ComponentType.js"
import LayoutStringParser from "../LayoutStringParser.js"
import ImageBuilder from "./ImageBuilder.js"
import Layout from "../Layout.js";
import MarkupBuilder from "./MarkupBuilder.js";
import TextBuilder from "./TextBuilder.js";
import TitleBuilder from "./TitleBuilder.js";

export default class PrismicAdapter {

    constructor() {
        this.imageBuilder = new ImageBuilder();
        this.markupBuilder = new MarkupBuilder();
        this.textBuilder = new TextBuilder();
        this.titleBuilder = new TitleBuilder();
    }

    adapt(prismicResponseData) {
        let layoutSet = [];
        let dataObjectMap = this.buildDataObjectMap(prismicResponseData);
        let layouts = this.findKeysByType(dataObjectMap, ComponentType.LAYOUT);
        layouts = this.sortKeys(layouts);
        layouts.forEach(function(layoutKey){
            Logger.info('Parsing Layout ' + layoutKey);
            let layout = new Layout();
            layout.key = layoutKey;
            layout.containers = LayoutStringParser.parseLayoutString(layoutKey, dataObjectMap);
            layoutSet.push(layout);
        });
        return layoutSet;
    }

    findKeysByType(dataObjectMap, type){
        let normalizedType;
        let matched = [];
        for (let typeKey in dataObjectMap) {
            if(ComponentType.normalize(typeKey) == type){
                matched.push(typeKey);
            }
        }
        return matched;
    }

    sortKeys(keys) {
        return keys.sort(function(a,b){
            return a.localeCompare(b);
        });
    }

    /**
     * Create a mapping of keys to data objects.
     * @param prismicResponseData
     */
    buildDataObjectMap(prismicResponseData) {
        let dataObject;
        let map = {};
        for(let typeKey in prismicResponseData) {
            if (prismicResponseData[typeKey]) {
                dataObject = this.buildDataObject(typeKey, prismicResponseData[typeKey]);
                if (dataObject) {
                    map[typeKey] = dataObject;
                }
            }
        }
        return map;
    }

    buildDataObject(typeKey, nodeData) {
        let dataObject;
        let type = ComponentType.normalize(typeKey);
        switch (type) {
            case ComponentType.LAYOUT:
                dataObject = nodeData;
                break;
            case ComponentType.IMAGE:
                dataObject = this.buildImage(nodeData);
                break;
            case ComponentType.TITLE:
                dataObject = this.buildTitle(nodeData);
                break;
            case ComponentType.TEXT:
            case ComponentType.MARKUP:
                dataObject = this.buildMarkup(nodeData);
                break;
            default: Logger.warning('Unknown component type "' + type + '"');
        }
        return dataObject;
    }

    buildImage(data){
        this.imageBuilder.init();
        if (data.dimensions) {
            this.imageBuilder.withSourceAndSize(data.url, data.dimensions.width, data.dimensions.height);
        }
        if (data.mobile && data.mobile.dimensions) {
            this.imageBuilder.withSourceAndSize(data.mobile.url, data.mobile.dimensions.width, data.mobile.dimensions.height);
        }
        if (data.tablet && data.tablet.dimensions) {
            this.imageBuilder.withSourceAndSize(data.tablet.url, data.tablet.dimensions.width, data.tablet.dimensions.height);
        }
        if (data.desktop && data.desktop.dimensions) {
            this.imageBuilder.withSourceAndSize(data.desktop.url, data.desktop.dimensions.width, data.desktop.dimensions.height);
        }
        return this.imageBuilder.build();
    }

    buildText(data){
        this.textBuilder.init();
        for(let key in data) {
            let value = data[key];
            if ('paragraph' == value.type) {
                this.textBuilder.withParagraph(value.text);
            }
        }
        return this.textBuilder.build();
    }

    buildMarkup(data){
        this.markupBuilder.init();
        for(let key in data) {
            let value = data[key];
            if ('paragraph' == value.type) {
                this.markupBuilder.withParagraphAndSpans(value.text, value.spans);
            }
        }
        return this.markupBuilder.build();
    }

    buildTitle(data){
        let titleData = data.pop();
        this.titleBuilder.init();
        switch(titleData.type){
            case 'heading1':
                this.titleBuilder.withHeader1(titleData.text);
                break;
            case 'heading2':
                this.titleBuilder.withHeader2(titleData.text);
                break;
            case 'heading3':
                this.titleBuilder.withHeader3(titleData.text);
                break;
            case 'heading4':
                this.titleBuilder.withHeader4(titleData.text);
                break;
        }
        return this.titleBuilder.build();
    }
}
