import Title from '../Types/Title.js'

export default class MarkupBuilder {

    constructor() {
        this.init();
    }

    init() {
        this.title = new Title();
    }

    withHeader1(content){
        this.title.tag = 'h1';
        this.title.content = content;
    }

    withHeader2(content){
        this.title.tag = 'h2';
        this.title.content = content;
    }

    withHeader3(content){
        this.title.tag = 'h3';
        this.title.content = content;
    }

    withHeader4(content){
        this.title.tag = 'h4';
        this.title.content = content;
    }

    build() {
        return this.title;
    }
}
