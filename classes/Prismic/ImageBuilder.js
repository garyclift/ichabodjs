import Image from '../Types/Image.js'
import ImageSet from '../Types/ImageSet.js'

export default class ImageBuilder {
    constructor(){
        this.init();
    }

    init(){
        this.altText = '';
        this.sourceAndSizes = [];
        return this;
    }

    withAltText(altText) {
        this.altText = altText;
        return this;
    }

    withSourceAndSize(source, width, height) {
        this.sourceAndSizes.push({src:source, width: width, height: height});
        return this;
    }

    build() {
        let image;
        if(this.sourceAndSizes && 1 < this.sourceAndSizes.length){
            image = new ImageSet();
            this.sourceAndSizes.forEach(function(value){
                let child = new Image();
                child.width = value.width;
                child.height = value.height;
                child.src = value.src;
                image.addImage(child);
            });
        } else if(this.sourceAndSizes && 0 < this.sourceAndSizes.length){
            image = new Image();
            let seourceAndSize = this.sourceAndSizes.pop();
            image.src = seourceAndSize.src;
            if(seourceAndSize.width) {
                image.width = seourceAndSize.width;
            }
            if (seourceAndSize.height) {
                image.height = seourceAndSize.height;
            }
        } else {
            throw 'I\'m sorry but I cannot build an image without a source and size.';
        }
        if(this.altText && 0 < this.altText.length) {
            image.altText = this.altText;
        }
        return image;
    }
}
