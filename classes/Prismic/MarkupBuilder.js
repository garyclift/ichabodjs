import Markup from '../Types/Markup.js'
import Paragraph from "../Types/Paragraph.js";
import Span from "../Types/Span.js";

export default class MarkupBuilder {

    constructor() {
        this.init();
    }

    init() {
        this.markup = new Markup();
        return this;
    }

    withParagraph(content){
        let child = new Paragraph();
        child.content = content;
        this.markup.addChild(child);
        return this;
    }

    withParagraphAndSpans(content, spans){
        let child = new Paragraph();
        let span;
        child.content = content;
        for(let i = 0; i < spans.length; i++) {
            span = new Span();
            span.addClass(spans[i].type)
            span.start = spans[i].start;
            span.end = spans[i].end;
            span.content = content.substring(span.start, span.end);
            child.addSpan(span);
        }
        this.markup.addChild(child);
        return this;
    }

    build() {
        return this.markup;
    }
}
