import Logger from "../Logger.js";
import Variable from "../Utils/Variable.js";

export default class PrismicFacade {

    constructor(baseUrl) {
        this.baseUrl = baseUrl;
        this.searchPath = '/documents/search';
    };

    /**
     * Get the data from the Prismic Server.
     * @param searchParameters
     * @returns {Promise<unknown>}
     */
    getData(searchOptions) {
        let searchParameters = this.buildParameters(searchOptions);
        let urlPath = this.searchPath + '?' + this.getUrlParameters(searchParameters);
        try {
            let that = this;
            return new Promise(function (resolve, reject) {
                that.callServer(urlPath)
                    .then(function (data) {
                        let parsedResponse = JSON.parse(data.response);
                        if(parsedResponse.results || 0 == parsedResponse.results.length) {
                            let singleResult = parsedResponse.results.pop();
                            resolve(singleResult.data);
                        } else {
                            reject('I regret to inform you, no result was returned from the Prismic request.');
                        }
                    }, function(error){
                        Logger.error('Failed to get data from Prismic. Response Code "' + error.status + '" with message "' + error.statusText + '"');
                    });
            });
        } catch (error){
            Logger.error('Error Status ' + error.status + ' reason ' + error.statusText)
        }
    };


    /**
     * keys:
     *  - reference *required
     *  - version
     *  - searchContext (tag, type)
     *  === OR ==
     *  - type
     *  - tag
     * @param searchOptions
     */
    buildParameters(searchOptions){
        let parameters = {
            ref:'',
            q:[]
        };

        let prismicOptions = searchOptions.prismic;
        if(Variable.isEmpty(this.baseUrl)){
            throw 'Cannot build query options without the baseUrl.';
        }

        // FLATTEN SEARCH CONTEXT
        // WILL OVERWRITE CONFLICTING KEYS
        //.................................
        for(let k in searchOptions.searchContext){
            let val = searchOptions.searchContext[k];
            searchOptions[k]
        }
        delete searchOptions.searchContext;


        // SEARCH PREDS
        //.............
        for(let k in searchOptions){
            let val = searchOptions[k];
            switch(k){
                case 'type':
                    parameters.q.push("[[at(document.type,\"" + val + "\")]]");
                    break;
                case 'tag':
                    parameters.q.push("[[at(document.tags,[\"" + val + "\"])]]");
                    break;
                case 'reference':
                case 'version':
                    parameters.ref = val;
                    break;
            }
        }

        // VALIDATE PARAMETERS
        //....................
        if(Variable.isEmpty(parameters['ref'])){
            throw 'Cannot build query options without the reference.';
        }

        return parameters;
    };

    /**
     * Call the primic server with the url and method.
     * @param urlPath
     * @param method
     * @returns {Promise<unknown>}
     */
    callServer(urlPath, method) {
        let finalUrl = this.baseUrl + urlPath;
        let xhr = new XMLHttpRequest();
        return new Promise(function (resolve, reject) {
            xhr.onreadystatechange = function () {
                if (xhr.readyState !== 4) return;
                if (xhr.status >= 200 && xhr.status < 300) {
                    resolve(xhr);
                } else {
                    reject({
                        status: xhr.status,
                        statusText: xhr.statusText
                    });
                }
            }

            Logger.info("PrismicFacade Calling " + finalUrl);
            xhr.open(method || "GET", finalUrl, true);
            xhr.send();
        });
    };

    getUrlParameters(searchParameters){
        let urlParts = [];
        let part = null;
        for(let k in searchParameters){
            part = searchParameters[k];
            if(Array.isArray(part)){
                for(let i = 0; i < part.length; i++){
                    urlParts.push(k + '=' + part[i]);
                }
            } else {
                urlParts.push(k + '=' + searchParameters[k]);
            }
        }
        let params = urlParts.join('&');
        return encodeURI(params);
    };
}
