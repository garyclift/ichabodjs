import Container from "./Container.js";
import Component from "./Component.js";
import ComponentType from "./ComponentType.js";
import Logger from "./Logger.js";

/**
 * Delimiter for containers
 */
const CONTAINER_DELIM = '|';

/**
 * Delimiter for component
 */
const COMPONENT_DELIM = '.';

export default class LayoutStringParser {

    /**
     * Parse a layout string into Containers and Components.
     * @param {string} layoutString
     */
    static parseLayoutString(layoutKey, dataObjectMap) {
        let containers = [];
        let containerIndex = 0;
        let layoutString = dataObjectMap[layoutKey];
        layoutString.split(CONTAINER_DELIM).forEach(function (containerString) {
            let container = new Container();
            container.index = containerIndex;
            let componentStrings = containerString.split(COMPONENT_DELIM);
            let componentIndex = 0;
            componentStrings.forEach(function (componentKey) {
                if(undefined == dataObjectMap[componentKey]){
                    Logger.warning('No Object found with key "' + componentKey + '"');
                    return;
                }
                let component = new Component();
                component.index = componentIndex;
                try{
                    let normalizedType = ComponentType.normalize(componentKey);
                    component.type = normalizedType;
                    component.key = componentKey;
                    component.data = dataObjectMap[componentKey];
                    container.addComponent(component);
                } catch (e) {
                    Logger.error('Error Parsing Component "' + componentKey + '"');
                }
                componentIndex++;
            });
            // CONTAINERS REQUIRE COMPONENTS
            if(container.components.length) {
                containers.push(container);
            }
            containerIndex++;
        });
        return containers;
    }
}
