import LayoutStringParser from './LayoutStringParser.js';

export default class Layout {

    /**
     *
     * @param {string} layoutString
     */
    constructor(){
        this._key = '';
        this._containers = [];
    }

    get key() {
        return this._key;
    }

    set key(value) {
        this._key = value;
    }

    get containers() {
        return this._containers;
    }

    set containers(value) {
        this._containers = value;
    }
}
