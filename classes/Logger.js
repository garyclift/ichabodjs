
export default class Logger {

    static info(msg){
        console.log(msg);
    }

    static warning(msg){
        console.error(msg);
    }

    static error(msg){
        console.error(msg);
    }
}
