import TextLineBuilder from './TextLineBuilder.js'
import Logger from '../Logger.js'

export default class TextRenderer {

    constructor(){
        this.textLineBuilder = new TextLineBuilder(144, '-', '|', ' ');
    }

    render(layouts){
        let output = [];
        let textRenderer = this;

        let textLineBuilder = this.textLineBuilder;

        textLineBuilder.init();
        textLineBuilder.withHorizontalLine();
        let horizontalLine = textLineBuilder.build();

        textLineBuilder.init();
        textLineBuilder.withVerticalMarkAtStart();
        textLineBuilder.withVerticalMarkAtEnd();
        let blankLine = textLineBuilder.build();

        layouts.forEach(function(layout){
            output.push(horizontalLine)
            output.push(blankLine)
            textLineBuilder.init();
            textLineBuilder.withVerticalMarkAtStart();
            let containerWidth = Math.floor(textLineBuilder.width / layout.containers.length);
            for(let i = 0; i < layout.containers.length; i++) {
                let container = layout.containers[i];
                textLineBuilder.withVerticalMarkAtIndex(i * containerWidth);
                let componentWidth = Math.floor(containerWidth / container.components.length);
                for(let j = 0; j < container.components.length; j++){
                    let component = container.components[j];
                    let left = i * containerWidth + (j * componentWidth);
                    let right = i * containerWidth + ((j + 1) * componentWidth);
                    let center = left + ((right - left) / 2);
                    Logger.info('Layout ' + layout.key + ' ' + container.index + ' ' + component.type + ' ' + left + ',' + center + ',' + right);
                    textLineBuilder.withVerticalMarkAtIndex(right);
                    textLineBuilder.withStringCenteredAtIndex(component.type, center);
                }
            }
            textLineBuilder.withVerticalMarkAtEnd();
            let line = textLineBuilder.build();
            output.push(line);
            output.push(blankLine);
            output.push(horizontalLine);
        });
        return output.join("\n");
    }


}
