
import Logger from '../Logger.js'
import HtmlTagBuilder from "./HtmlTagBuilder.js";

export default class HtmlRenderer {

    constructor(){
        this.htmlTagBuilder = new HtmlTagBuilder();
    }

    render(layouts){
        let output = [];
        let builder = this.htmlTagBuilder;
        let layout;
        let layoutTag;
        let container;
        let containerTag;
        let component;
        let componentTag;
        let componentElement;
        let containerColumns = 12;
        let componentColumns = 12;
        let containerWidth;
        let componentWidth;
        for(let i = 0; i < layouts.length; i++){
            layout = layouts[i];
            layoutTag = builder.init()
                .withTag('div')
                .addClass('layout')
                .addClass('layout-' + i)
                .build();
            containerWidth = containerColumns / layout.containers.length;
            for(let j = 0; j < layout.containers.length; j++) {
                container = layout.containers[j];
                containerTag = builder.init()
                    .withTag('div')
                    .addClass('container')
                    .addClass('container-' + j)
                    .addClass('cols-' + containerWidth)
                    .build();
                componentWidth = componentColumns / container.components.length;
                for(let k = 0; k < container.components.length; k++){
                    component = container.components[k];
                    componentTag = builder.init()
                        .withTag('div')
                        .addClass('component')
                        .addClass('component-' + component.type)
                        .addClass('component-' + component.key)
                        .addClass('component-' + k)
                        .addClass('cols-' + componentColumns)
                        .build();
                    Logger.info('Tag: ' + component.data.tag);
                    componentElement = this.renderComponentTag(component.data);
                    componentTag.appendChild(componentElement);
                    containerTag.appendChild(componentTag);
                }
                layoutTag.appendChild(containerTag);
            }
            output.push(layoutTag);
        };
        return output;
    }

    renderComponentTag(element)
    {
        this.htmlTagBuilder.init().withElement(element);
        let tag = this.htmlTagBuilder.build();
        let child;
        for (let i = 0; i < element.children.length; i++) {
            element.children[i].addClass('child-' + i);
            child = this.renderComponentTag(element.children[i]);
            tag.appendChild(child);
        }
        return tag;
    }


}
