import Element from '../Types/Element.js'

export default class HtmlTagBuilder{

    constructor(){
        this.init();
    }

    init(){
        this.tag = '';
        this.id = '';
        this.classes = [];
        this.isSelfClosing = false;
        this.content = '';
        this.attributes = {};
        this.spans = [];
        return this;
    }

    withTag(tag){
        this.tag = tag;
        return this;
    }

    withId(id){
        this.id = id;
        return this;
    }

    withClasses(classes){
        this.classes = classes;
        return this;
    }

    addClass(cssClass){
        this.classes.push(cssClass);
        return this;
    }

    withIsSelfClosing(isSelfClosing){
        this.isSelfClosing = isSelfClosing;
        return this;
    }

    withContent(content){
        this.content = content;
        return this;
    }

    withAttributes(attributes){
        this.attributes = attributes;
        return this;
    }

    withAttribute(key, val){
        this.attributes[key] = val;
        return this;
    }

    /**
     *
     * @param {Element}element
     */
    withElement(element){
        this.tag = element.tag;
        this.id = element.id;
        this.classes = element.classes;
        this.isSelfClosing = element.selfClosing;
        this.content = element.content;
        this.attributes = element.attributes;
        this.spans = element.spans?element.spans:[];
    }

    build(){

        if(!this.tag || '' == this.tag){
            throw 'Cannot create tag without tag name';
        }
        //let element = document.createDocumentFragment();
        //element.tagName = this.tag;
        let element = document.createElement(this.tag);


        // CLASSES
        this.classes.forEach(function(cssClass){
            element.classList.add(cssClass);
        });

        // ID
        if(!this._isEmptyString(this.id)){
            element.id = this.id;
        }

        // ATTRIBUTES
        for(let k in this.attributes){
            element.setAttribute(k, this.attributes[k]);
        }

        // CONTENT
        if(!this.isSelfClosing) {
            if (typeof this.content === 'string' || this.content instanceof String) {
                if (!this._isEmptyString(this.content)) {
                    if(this.spans.length){
                        let content = this.content;
                        for(let i = 0; i < this.spans.length; i++){
                            let innerBuilder = new HtmlTagBuilder();
                            innerBuilder.init().withElement(this.spans[i]);
                            let span = innerBuilder.build();
                            content = content.substring(0,this.spans[i].start) +
                                span.outerHTML +
                                content.substring(this.spans[i].end);
                        }
                        element.innerHTML = content;
                    } else {
                        let textNode = document.createTextNode(this.content);
                        element.appendChild(textNode);
                    }
                }
            } else if (typeof this.content === 'Element') {
                let innerBuilder = new HtmlTagBuilder();
                innerBuilder.init().withElement(this.content);
                element.appendChild(innerBuilder.build());
            }
        }


        return element;

    }

    _isEmptyString(val){
        return !val || '' == val;
    }
}
