

export default class TextLineBuilder{
    constructor(width, horizontalChar, verticalChar, spaceChart){
        this.width = width;
        this.horizontalChar = horizontalChar;
        this.verticalChar = verticalChar;
        this.spaceChar = spaceChart;
    }

    init(){
        this.line = [];
        for(let i = 0; i < this.width; i++){
            this.line[i] = this.spaceChar;
        }
    }

    withCharAtIndex(char, idx){
        this.line[idx] = char;
    }

    withStringAtIndex(str, idx){
        let strArray = str.split('');
        for(let i = 0; i < strArray.length; i++){
            this.line[idx + i] = strArray[i];
        }
    }

    withStringCenteredAtIndex(str, centerIndex){
        let startIndex = Math.floor(centerIndex - (str.length / 2));
        this.withStringAtIndex(str, startIndex)
    }

    withHorizontalLine(){
        for(let i = 0; i < this.width; i++){
            if(this.spaceChar == this.line[i]) {
                this.line[i] = this.horizontalChar;
            }
        }
    }

    withVerticalMarkAtStart(){
        this.withVerticalMarkAtIndex(0);
    }

    withVerticalMarkAtEnd(){
        this.withVerticalMarkAtIndex(this.width - 1);
    }

    withVerticalMarkAtIndex(idx){
        if(idx < this.width && 0 <= idx) {
            this.line[idx] = this.verticalChar;
        }
    }

    build(){
        return this.line.join('');
    }
}
