
const CLASS_DELIM = '';

export default class Element{
    constructor(tag, selfClosing){
        this._id = '';
        this._classes = [];
        this.selfClosing = false;
        this._content = '';
        if(selfClosing){
            this.selfClosing = true;
        }
        this._children = [];
        this._tag = tag;
        this._selfClosing = selfClosing;
    }

    get tag() {
        return this._tag;
    }

    set tag(value) {
        this._tag = value;
    }

    get selfClosing() {
        return this._selfClosing;
    }

    set selfClosing(value) {
        this._selfClosing = value;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get classes() {
        return this._classes;
    }

    set classes(value) {
        this._classes = value;
    }

    addClass(value) {
        this._classes.push(value);
    }

    get content() {
        return this._content;
    }

    set content(value) {
        this._content = value;
    }

    get attributes() {
        let attributes = this.getAutoAttributes(['id']);
        if(this._classes && 0 < this._classes.length){
            attributes.class = this._classes.join(CLASS_DELIM);
        }
        return attributes;
    }

    /**
     * Get the child elements of this element.
     * @returns {[]|Array}
     */
    get children() {
        return this._children;
    }

    set children(value) {
        this._children = value;
    }

    addChild(value) {
        this._children.push(value);
    }

    /**
     *
     * @param {[]/Array} properyNames
     * @private
     */
    getAutoAttributes(properyNames){
        let attributes = {};
        let element = this;
        properyNames.forEach(function(value) {
            if (element[value]) {
                attributes[value] = element[value];
            } else if (element['_' + value]) {
                attributes[value] = element['_' + value];
            }
        });
        return attributes;
    }
}
