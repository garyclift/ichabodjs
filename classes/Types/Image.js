
import Element from './Element.js'

export default class Image extends Element {
    constructor() {
        super('img', true);
        this._width = 0;
        this._height = 0;
        this._src = '';
        this._altText = '';
    }

    get width() {
        return this._width;
    }

    set width(value) {
        this._width = value;
    }

    get height() {
        return this._height;
    }

    set height(value) {
        this._height = value;
    }

    get src() {
        return this._src;
    }

    set src(value) {
        this._src = value;
    }

    get altText() {
        return this._altText;
    }

    set altText(value) {
        this._altText = value;
    }

    get attributes() {
        let attributes = super.attributes;
        let selfAttributes = super.getAutoAttributes(['width', 'height', 'src', 'altText']);
        return Object.assign(attributes, selfAttributes);
    }
}
