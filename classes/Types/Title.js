
import Element from './Element.js'

export default class Title extends Element {
    constructor() {
        super('div', false);
        this._content = '';
    }
}
