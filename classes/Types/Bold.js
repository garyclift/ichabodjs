import Element from './Element.js'

export default class Bold extends Element{

    constructor(){
        super('b');
        this._content = '';
    }

    get content() {
        return this._content;
    }

    set content(value) {
        this._content = value;
    }
}
