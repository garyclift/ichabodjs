import Element from "./Element.js";

export default class Span extends Element{
    constructor(){
        super('span');
        this._content = '';
        this._start = -1;
        this._end = -1;
    }

    get content() {
        return this._content;
    }

    set content(value) {
        this._content = value;
    }

    get start() {
        return this._start;
    }

    set start(value) {
        this._start = value;
    }

    get end() {
        return this._end;
    }

    set end(value) {
        this._end = value;
    }
}
