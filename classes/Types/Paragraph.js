import Element from './Element.js'

export default class Paragraph extends Element{

    constructor(){
        super('p');
        this._content = '';
        this._spans = [];
    }

    get content() {
        return this._content;
    }

    set content(value) {
        this._content = value;
    }


    get spans() {
        return this._spans;
    }

    set spans(value) {
        this._spans = value;
    }

    addSpan(value) {
        this._spans.push(value);
    }
}
