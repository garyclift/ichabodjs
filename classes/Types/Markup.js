import Element from './Element.js'

export default class Markup extends Element{

    constructor(){
        super('div');
        this.addClass('markup');
        this._content = '';
    }

    get content() {
        return this._content;
    }

    set content(value) {
        this._content = value;
    }
}
