import Image from './Image.js'
import Element from './Element.js'

export default class ImageSet extends Element{

    constructor(){
        super('img', true);
        this._images = [];
        this._altText = '';
    }

    get images() {
        return this._images;
    }

    set images(value) {
        this._images = value;
    }

    addImage(image) {
        this._images.push(image);
    }

    get altText() {
        return this._altText;
    }

    set altText(value) {
        this._altText = value;
    }

    get attributes() {
        let attributes = super.attributes;
        let selfAttributes = super.getAutoAttributes(['altText']);
        let srcSet = [];
        this._images.forEach(function(value){
            srcSet.push(value.src + ' ' + value.width + 'w');
        });
        selfAttributes.srcset = srcSet.join(',');
        return Object.assign(attributes, selfAttributes);
    }
}
