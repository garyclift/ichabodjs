export default class ComponentType {

    /**
     * A layout defines the organization of containers
     * and components
     * @type {string}
     */
    static get LAYOUT(){
        return 'layout';
    };

    /**
     * Represents an image
     * @type {string}
     */
    static get IMAGE(){
        return 'image';
    };

    /**
     * Represents a title
     * @type {string}
     */
    static get TITLE(){
        return 'title';
    };

    /**
     * Represent markup
     * @type {string}
     */
    static get MARKUP(){
        return 'markup';
    };

    /**
     * Represents text
     * @type {string}
     */
    static get TEXT(){
        return 'text';
    };

    /**
     * ALIAS = VALUE
     * h1 = title
     * h2 = title
     *
     * @returns {{img: *}}
     * @constructor
     */
    static get ALIAS(){
        return {
            'img':ComponentType.IMAGE
        };
    };

    /**
     * Get the normalized type.
     * @param type
     * @returns {string|*}
     */
    static normalize(type){
        type = type.toLowerCase().replace(/[^a-z]/gi, '')
        switch(type){
            case ComponentType.LAYOUT: return type;
            case ComponentType.IMAGE: return type;
            case ComponentType.TITLE: return type;
            case ComponentType.MARKUP: return type;
            case ComponentType.TEXT: return type;
        }
        if(ComponentType.ALIAS[type]){
            return ComponentType.ALIAS[type];
        }
        throw 'Cannot normalize type "' + type + '".';
    }
}
