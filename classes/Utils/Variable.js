

export default class Variable{

    static isEmpty(val){
        if(val){
            if (typeof this.content === 'string' || this.content instanceof String) {
                return isEmptyString(val);
            }
        }
    }

    static isEmptyString(str){
        return !str || '' === str;
    }
}
