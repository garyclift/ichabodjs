import Logger from '../Logger.js'
export default class Timer {
    static time(label) {
        let current = Date.now();
        if (Timer.lastTime) {
            let delta = current - Timer.lastTime;
            Logger.info("Time for " + label + " = " + delta);
        }
        Timer.lastTime = current;
    }
}
