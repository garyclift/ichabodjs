import PrismicFacade from "./Prismic/PrismicFacade.js";
import Variable from "./Utils/Variable.js";
import PrismicAdapter from "./Prismic/PrismicAdapter.js";
import Logger from "./Logger.js";
import Timer from "./Utils/Timer.js";
import HtmlRenderer from "./Renderers/HtmlRenderer.js";

export default class Ichabod{

    constructor(contextOptions){
        let deaultOptions = {
            prismic: {
                reference:'',
                baseUrl:'',
                searchPath:'/documents/search'
            },
            language:'',
            version:''
        };
        this.options = Object.assign(deaultOptions, contextOptions);
        this.facade = this._getFacade(this.options);        // get data
        this.adapter = this._getAdapter(this.options);      // convert to layouts
        this.renderer = this._getRenderer(this.options);    // render layouts to nodes
    }

    injectContent(searchContext, targetSelector){
        Timer.time('Start');
        let searchOptions = Object.assign(searchContext, this.options);
        let that = this;
        this.facade.getData(searchOptions)
            // RENDER DATA
            .then(function(rawData){
                Timer.time('Render Start');
                let layouts = that.adapter.adapt(rawData);
                Logger.info(layouts);
                return new Promise(function (resolve, reject) {
                    if(layouts || 0 == layouts.results.length) {
                        Timer.time('Render End');
                        resolve(layouts);
                    } else {
                        reject('Oh no! No rendered layouts to inject.');
                    }
                });
            })
            // INJECT DATA
            .then(function(layouts){
                Timer.time('Inject Start');
                //let renderer = new TextRenderer();
                let nodes = that.renderer.render(layouts);
                Logger.info('Getting Target "' + targetSelector + '"');
                let parent = document.getElementById(targetSelector);
                if(!parent){
                    Logger.warning('Element "' + targetSelector +'" not found.');
                };
                nodes.forEach(function(node){
                    parent.appendChild(node);
                });
                Timer.time('Inject End');
            });
    }

    injectLayouts(data){
        let target = data.target;
        let layouts = data.layout;
        //let renderer = new TextRenderer();
        let nodes = this.renderer.render(layouts);
        let parent = document.getElementById(target);
        nodes.forEach(function(node){
            parent.appendChild(node);
        });
    }

    _getFacade(options){
        let facade = null;
        if(options.prismic){
            if(Variable.isEmpty(options.prismic.baseUrl)){
                throw 'When using the Prismic module, the baseUrl variable is required.';
            } else if(Variable.isEmpty(options.prismic.ref)){
                throw 'When using the Prismic module, the ref variable is required.';
            }
            facade = new PrismicFacade(options.prismic.baseUrl);
        }
        return facade;
    }

    _getAdapter(options){
        let adapter = null;
        if(options.prismic){
            adapter = new PrismicAdapter();
        }
        return adapter;
    }

    _getRenderer(options){
        return new HtmlRenderer();
    }
}
