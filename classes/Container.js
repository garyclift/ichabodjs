
/**
 * A Container is rendered into markup. It
 * also contains components which are also
 * rendered.
 */
export default class Container{
    /**
     * Non-Valued constructor
     */
    constructor(){
        this._layout = '';
        this._index = 0;
        this._components = [];
    }

    get layout() {
        return this._layout;
    }

    set layout(value) {
        this._layout = value;
    }

    get index() {
        return this._index;
    }

    set index(value) {
        this._index = value;
    }

    /**
     * Get the components in this container.
     * @returns {[]|Array}
     */
    get components() {
        return this._components;
    }

    /**
     * Set the components in this container
     * @param value
     */
    set components(value) {
        this._components = value;
    }

    /**
     * Add a component to this container.
     * @param value
     */
    addComponent(value) {
        this._components.push(value);
    }
}
