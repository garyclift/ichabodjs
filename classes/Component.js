/**
 * A component is an individual item
 * that is rendered into markup.
 */
export default class Component {
    /**
     * Non-Valued constructor
     */
    constructor() {
        this._key = '';
        this._index = 0;
        this._type = '';
        this._data = {};
    }


    get key() {
        return this._key;
    }

    set key(value) {
        this._key = value;
    }

    get index() {
        return this._index;
    }

    set index(value) {
        this._index = value;
    }

    /**
     * Get the type of this component.
     * Example = title or image
     * @returns {[]|Array}
     */
    get type() {
        return this._type;
    }

    /**
     * Set the type of this container.
     * @param value
     */
    set type(value) {
        this._type = value;
    }

    get data() {
        return this._data;
    }

    set data(value) {
        this._data = value;
    }

}
