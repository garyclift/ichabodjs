
import Ichabod from './classes/Ichabod.js';

// VARIABLES
//...........
let version = 'XYfqaBEAACUAg2ul';
let tag = 'COCHAWVLGOBRXXXXX';
let contentType = 'brewery-shirt';
let baseUrl = 'https://davesbeergear.cdn.prismic.io/api/v2';
let locale = 'en_us';

// GLOBAL OPTIONS FOR ALL CONTENT
//.................................
let options = {
    prismic: {
        baseUrl: baseUrl
    },
    version: version,
    language:'en_us'
}
let ichabod = new Ichabod(options);


// INJECTION 1 - SEARCH BY TAG AND CONTENT TYPE
ichabod.injectContent({tag: tag, type:contentType}, "ichabod");

// INJECTION 2 - SEARCH BY TAG
ichabod.injectContent({tag: tag}, "ichabod2");
