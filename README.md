# README #

ICHABODjs is a thin Javascript library available to pull headless content and inject the content into a local page.

### What is this repository for? ###

* Stores the thin Javascript library called ICHABODjs 
* Version 1.0

### How do I get set up? ###

ICHABODjs is a simple library with no external dependencies. To use include one of the files in the `dist` folder in the html file. Then add a simple set of options and instantiate the Ichabod class and run the `go` function.

```javascript
let version = 'PRISMIC CONTENT VERSION';
let tag = 'TAG FOR SEARCH';
let contentType = 'PRISMIC CONTENT TYPE';


let options = {
    prismic: {
        type: contentType,
        baseUrl: 'PRISMIC BASE URL'
    }
}

let ichabod = new Ichabod(options);
ichabod.go(tag, version);
````

### Contribution guidelines ###

* We need help developing classes for Headless CMS systems.
* Please feel free to offer suggestions on how to best organize code or make the code run faster.


### Who do I talk to? ###

* gclift+ichabodjs@gmail.com
