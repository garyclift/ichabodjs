
//import Ichabod from './classes/Ichabod.js';

let version = 'XXWozxEAACgAMYRv';
let tag = 'COBRAMTSUPABXXXXX';
let contentType = 'brewery-shirt';


let options = {
    prismic: {
        type: contentType,
        baseUrl: 'https://davesbeergear.cdn.prismic.io/api/v2'
    }
}

let ichabod = new Ichabod(options);
ichabod.go(tag, version);

