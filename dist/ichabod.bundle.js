var Ichabod = (function () {
    'use strict';

    class Logger {

        static info(msg){
            console.log(msg);
        }

        static warning(msg){
            console.error(msg);
        }

        static error(msg){
            console.error(msg);
        }
    }

    class Variable{

        static isEmpty(val){
            if(val){
                if (typeof this.content === 'string' || this.content instanceof String) {
                    return isEmptyString(val);
                }
            }
        }

        static isEmptyString(str){
            return !str || '' === str;
        }
    }

    class PrismicFacade {

        constructor(baseUrl) {
            this.baseUrl = baseUrl;
            this.searchPath = '/documents/search';
        };

        /**
         * Get the data from the Prismic Server.
         * @param searchParameters
         * @returns {Promise<unknown>}
         */
        getData(searchOptions) {
            let searchParameters = this.buildParameters(searchOptions);
            let urlPath = this.searchPath + '?' + this.getUrlParameters(searchParameters);
            try {
                let that = this;
                return new Promise(function (resolve, reject) {
                    that.callServer(urlPath)
                        .then(function (data) {
                            Logger.info(data.response);
                            let parsedResponse = JSON.parse(data.response);
                            if(parsedResponse.results || 0 == parsedResponse.results.length) {
                                let singleResult = parsedResponse.results.pop();
                                resolve(singleResult.data);
                            } else {
                                reject('I regret to inform you, no result was returned from the Prismic request.');
                            }
                        }, function(error){
                            Logger.error('Failed to get data from Prismic. Response Code "' + error.status + '" with message "' + error.statusText + '"');
                        });
                });
            } catch (error){
                Logger.error('Error Status ' + error.status + ' reason ' + error.statusText);
            }
        };


        /**
         * keys:
         *  - reference *required
         *  - version
         *  - searchContext (tag, type)
         *  === OR ==
         *  - type
         *  - tag
         * @param searchOptions
         */
        buildParameters(searchOptions){
            let parameters = {
                ref:'',
                q:[]
            };
            // %5B%5Bat(document.type,%20%22brewery-shirt%22)%5D%5D&q=%5B%5Bat(document.tags,%20%5B%22COBRAMTSUPABXXXXX%22%5D)%5D%5D&ref=' + reference

            let prismicOptions = searchOptions.prismic;
            if(Variable.isEmpty(this.baseUrl)){
                throw 'Cannot build query options without the baseUrl.';
            }

            // FLATTEN SEARCH CONTEXT
            // WILL OVERWRITE CONFLICTING KEYS
            //.................................
            for(let k in searchOptions.searchContext){
                let val = searchOptions.searchContext[k];
                searchOptions[k];
            }
            delete searchOptions.searchContext;


            // SEARCH PREDS
            //.............
            for(let k in searchOptions){
                let val = searchOptions[k];
                switch(k){
                    case 'type':
                        parameters.q.push("[[at(document.type,\"" + val + "\")]]");
                        break;
                    case 'tag':
                        parameters.q.push("[[at(document.tags,[\"" + val + "\"])]]");
                        break;
                    case 'reference':
                    case 'version':
                        parameters.ref = val;
                        break;
                }
            }

            // VALIDATE PARAMETERS
            //....................
            if(Variable.isEmpty(parameters['ref'])){
                throw 'Cannot build query options without the reference.';
            }

            return parameters;
        };

        /**
         * Call the primic server with the url and method.
         * @param urlPath
         * @param method
         * @returns {Promise<unknown>}
         */
        callServer(urlPath, method) {
            let finalUrl = this.baseUrl + urlPath;
            let xhr = new XMLHttpRequest();
            return new Promise(function (resolve, reject) {
                xhr.onreadystatechange = function () {
                    if (xhr.readyState !== 4) return;
                    if (xhr.status >= 200 && xhr.status < 300) {
                        resolve(xhr);
                    } else {
                        reject({
                            status: xhr.status,
                            statusText: xhr.statusText
                        });
                    }
                };

                Logger.info("PrismicFacade Calling " + finalUrl);
                xhr.open(method || "GET", finalUrl, true);
                xhr.send();
            });
        };

        getUrlParameters(searchParameters){
            let urlParts = [];
            let part = null;
            for(let k in searchParameters){
                part = searchParameters[k];
                if(Array.isArray(part)){
                    for(let i = 0; i < part.length; i++){
                        urlParts.push(k + '=' + part[i]);
                    }
                } else {
                    urlParts.push(k + '=' + searchParameters[k]);
                }
            }
            let params = urlParts.join('&');
            return encodeURI(params);
        };
    }

    class ComponentType {

        /**
         * A layout defines the organization of containers
         * and components
         * @type {string}
         */
        static get LAYOUT(){
            return 'layout';
        };

        /**
         * Represents an image
         * @type {string}
         */
        static get IMAGE(){
            return 'image';
        };

        /**
         * Represents a title
         * @type {string}
         */
        static get TITLE(){
            return 'title';
        };

        /**
         * Represent markup
         * @type {string}
         */
        static get MARKUP(){
            return 'markup';
        };

        /**
         * Represents text
         * @type {string}
         */
        static get TEXT(){
            return 'text';
        };

        /**
         * ALIAS = VALUE
         * h1 = title
         * h2 = title
         *
         * @returns {{img: *}}
         * @constructor
         */
        static get ALIAS(){
            return {
                'img':ComponentType.IMAGE
            };
        };

        /**
         * Get the normalized type.
         * @param type
         * @returns {string|*}
         */
        static normalize(type){
            type = type.toLowerCase().replace(/[^a-z]/gi, '');
            switch(type){
                case ComponentType.LAYOUT: return type;
                case ComponentType.IMAGE: return type;
                case ComponentType.TITLE: return type;
                case ComponentType.MARKUP: return type;
                case ComponentType.TEXT: return type;
            }
            if(ComponentType.ALIAS[type]){
                return ComponentType.ALIAS[type];
            }
            throw 'Cannot normalize type "' + type + '".';
        }
    }

    /**
     * A Container is rendered into markup. It
     * also contains components which are also
     * rendered.
     */
    class Container{
        /**
         * Non-Valued constructor
         */
        constructor(){
            this._layout = '';
            this._index = 0;
            this._components = [];
        }

        get layout() {
            return this._layout;
        }

        set layout(value) {
            this._layout = value;
        }

        get index() {
            return this._index;
        }

        set index(value) {
            this._index = value;
        }

        /**
         * Get the components in this container.
         * @returns {[]|Array}
         */
        get components() {
            return this._components;
        }

        /**
         * Set the components in this container
         * @param value
         */
        set components(value) {
            this._components = value;
        }

        /**
         * Add a component to this container.
         * @param value
         */
        addComponent(value) {
            this._components.push(value);
        }
    }

    /**
     * A component is an individual item
     * that is rendered into markup.
     */
    class Component {
        /**
         * Non-Valued constructor
         */
        constructor() {
            this._key = '';
            this._index = 0;
            this._type = '';
            this._data = {};
        }


        get key() {
            return this._key;
        }

        set key(value) {
            this._key = value;
        }

        get index() {
            return this._index;
        }

        set index(value) {
            this._index = value;
        }

        /**
         * Get the type of this component.
         * Example = title or image
         * @returns {[]|Array}
         */
        get type() {
            return this._type;
        }

        /**
         * Set the type of this container.
         * @param value
         */
        set type(value) {
            this._type = value;
        }

        get data() {
            return this._data;
        }

        set data(value) {
            this._data = value;
        }

    }

    /**
     * Delimiter for containers
     */
    const CONTAINER_DELIM = '|';

    /**
     * Delimiter for component
     */
    const COMPONENT_DELIM = '.';

    class LayoutStringParser {

        /**
         * Parse a layout string into Containers and Components.
         * @param {string} layoutString
         */
        static parseLayoutString(layoutKey, dataObjectMap) {
            let containers = [];
            let containerIndex = 0;
            let layoutString = dataObjectMap[layoutKey];
            layoutString.split(CONTAINER_DELIM).forEach(function (containerString) {
                let container = new Container();
                container.index = containerIndex;
                let componentStrings = containerString.split(COMPONENT_DELIM);
                let componentIndex = 0;
                componentStrings.forEach(function (componentKey) {
                    if(undefined == dataObjectMap[componentKey]){
                        Logger.warning('No Object found with key "' + componentKey + '"');
                        return;
                    }
                    let component = new Component();
                    component.index = componentIndex;
                    try{
                        let normalizedType = ComponentType.normalize(componentKey);
                        component.type = normalizedType;
                        component.key = componentKey;
                        component.data = dataObjectMap[componentKey];
                        container.addComponent(component);
                    } catch (e) {
                        Logger.error('Error Parsing Component "' + componentKey + '"');
                    }
                    componentIndex++;
                });
                // CONTAINERS REQUIRE COMPONENTS
                if(container.components.length) {
                    containers.push(container);
                }
                containerIndex++;
            });
            return containers;
        }
    }

    const CLASS_DELIM = '';

    class Element{
        constructor(tag, selfClosing){
            this._id = '';
            this._classes = [];
            this.selfClosing = false;
            this._content = '';
            if(selfClosing){
                this.selfClosing = true;
            }
            this._tag = tag;
            this._selfClosing = selfClosing;
        }

        get tag() {
            return this._tag;
        }

        set tag(value) {
            this._tag = value;
        }

        get selfClosing() {
            return this._selfClosing;
        }

        set selfClosing(value) {
            this._selfClosing = value;
        }

        get id() {
            return this._id;
        }

        set id(value) {
            this._id = value;
        }

        get classes() {
            return this._classes;
        }

        set classes(value) {
            this._classes = value;
        }

        get content() {
            return this._content;
        }

        set content(value) {
            this._content = value;
        }

        get attributes() {
            let attributes = this.getAutoAttributes(['id']);
            if(this._classes && 0 < this._classes.length){
                attributes.class = this._classes.join(CLASS_DELIM);
            }
            return attributes;
        }

        /**
         *
         * @param {[]/Array} properyNames
         * @private
         */
        getAutoAttributes(properyNames){
            let attributes = {};
            let element = this;
            properyNames.forEach(function(value) {
                if (element[value]) {
                    attributes[value] = element[value];
                } else if (element['_' + value]) {
                    attributes[value] = element['_' + value];
                }
            });
            return attributes;
        }
    }

    class Image extends Element {
        constructor() {
            super('img', true);
            this._width = 0;
            this._height = 0;
            this._src = '';
            this._altText = '';
        }

        get width() {
            return this._width;
        }

        set width(value) {
            this._width = value;
        }

        get height() {
            return this._height;
        }

        set height(value) {
            this._height = value;
        }

        get src() {
            return this._src;
        }

        set src(value) {
            this._src = value;
        }

        get altText() {
            return this._altText;
        }

        set altText(value) {
            this._altText = value;
        }

        get attributes() {
            let attributes = super.attributes;
            let selfAttributes = super.getAutoAttributes(['width', 'height', 'src', 'altText']);
            return Object.assign(attributes, selfAttributes);
        }
    }

    class ImageSet extends Element{

        constructor(){
            super('img', true);
            this._images = [];
            this._altText = '';
        }

        get images() {
            return this._images;
        }

        set images(value) {
            this._images = value;
        }

        addImage(image) {
            this._images.push(image);
        }

        get altText() {
            return this._altText;
        }

        set altText(value) {
            this._altText = value;
        }

        get attributes() {
            let attributes = super.attributes;
            let selfAttributes = super.getAutoAttributes(['altText']);
            let srcSet = [];
            this._images.forEach(function(value){
                srcSet.push(value.src + ' ' + value.width + 'w');
            });
            selfAttributes.srcset = srcSet.join(',');
            return Object.assign(attributes, selfAttributes);
        }
    }

    class ImageBuilder {
        constructor(){
            this.init();
        }

        init(){
            this.altText = '';
            this.sourceAndSizes = [];
            return this;
        }

        withAltText(altText) {
            this.altText = altText;
            return this;
        }

        withSourceAndSize(source, width, height) {
            this.sourceAndSizes.push({src:source, width: width, height: height});
            return this;
        }

        build() {
            let image;
            if(this.sourceAndSizes && 1 < this.sourceAndSizes.length){
                image = new ImageSet();
                this.sourceAndSizes.forEach(function(value){
                    let child = new Image();
                    child.width = value.width;
                    child.height = value.height;
                    child.src = value.src;
                    image.addImage(child);
                });
            } else if(this.sourceAndSizes && 0 < this.sourceAndSizes.length){
                image = new Image();
                let seourceAndSize = this.sourceAndSizes.pop();
                image.src = seourceAndSize.src;
                if(seourceAndSize.width) {
                    image.width = seourceAndSize.width;
                }
                if (seourceAndSize.height) {
                    image.height = seourceAndSize.height;
                }
            } else {
                throw 'I\'m sorry but I cannot build an image without a source and size.';
            }
            if(this.altText && 0 < this.altText.length) {
                image.altText = this.altText;
            }
            return image;
        }
    }

    class Layout {

        /**
         *
         * @param {string} layoutString
         */
        constructor(){
            this._key = '';
            this._containers = [];
        }

        get key() {
            return this._key;
        }

        set key(value) {
            this._key = value;
        }

        get containers() {
            return this._containers;
        }

        set containers(value) {
            this._containers = value;
        }
    }

    class Markup extends Element{

        constructor(){
            super('div');
            this._content = '';
        }

        get content() {
            return this._content;
        }

        set content(value) {
            this._content = value;
        }
    }

    class MarkupBuilder {

        constructor() {
            this.init();
        }

        init() {
            this.markup = new Markup();
            return this;
        }

        withParagraph(content){
            let currentContent = this.markup.content;
            this.markup.content = currentContent + '<p>' + content + '</p>';
            return this;
        }

        build() {
            return this.markup;
        }
    }

    class TextBuilder {

        constructor() {
            this.init();
        }

        init() {
            this.markup = new Markup();
        }

        withParagraph(content){
            let currentContent = this.markup.content;
            this.markup.content = currentContent + content;    }

        build() {
            return this.markup;
        }
    }

    class Title extends Element {
        constructor() {
            super('div', false);
            this._content = '';
        }
    }

    class MarkupBuilder$1 {

        constructor() {
            this.init();
        }

        init() {
            this.title = new Title();
        }

        withHeader1(content){
            this.title.tag = 'h1';
            this.title.content = content;
        }

        withHeader2(content){
            this.title.tag = 'h2';
            this.title.content = content;
        }

        withHeader3(content){
            this.title.tag = 'h3';
            this.title.content = content;
        }

        withHeader4(content){
            this.title.tag = 'h4';
            this.title.content = content;
        }

        build() {
            return this.title;
        }
    }

    class PrismicAdapter {

        constructor() {
            this.imageBuilder = new ImageBuilder();
            this.markupBuilder = new MarkupBuilder();
            this.textBuilder = new TextBuilder();
            this.titleBuilder = new MarkupBuilder$1();
        }

        adapt(prismicResponseData) {
            let layoutSet = [];
            let dataObjectMap = this.buildDataObjectMap(prismicResponseData);
            let layouts = this.findKeysByType(dataObjectMap, ComponentType.LAYOUT);
            layouts = this.sortKeys(layouts);
            layouts.forEach(function(layoutKey){
                Logger.info('Parsing Layout ' + layoutKey);
                let layout = new Layout();
                layout.key = layoutKey;
                layout.containers = LayoutStringParser.parseLayoutString(layoutKey, dataObjectMap);
                layoutSet.push(layout);
            });
            return layoutSet;
        }

        findKeysByType(dataObjectMap, type){
            let matched = [];
            for (let typeKey in dataObjectMap) {
                if(ComponentType.normalize(typeKey) == type){
                    matched.push(typeKey);
                }
            }
            return matched;
        }

        sortKeys(keys) {
            return keys.sort(function(a,b){
                return a.localeCompare(b);
            });
        }

        /**
         * Create a mapping of keys to data objects.
         * @param prismicResponseData
         */
        buildDataObjectMap(prismicResponseData) {
            let dataObject;
            let map = {};
            for(let typeKey in prismicResponseData) {
                if (prismicResponseData[typeKey]) {
                    dataObject = this.buildDataObject(typeKey, prismicResponseData[typeKey]);
                    if (dataObject) {
                        map[typeKey] = dataObject;
                    }
                }
            }
            return map;
        }

        buildDataObject(typeKey, nodeData) {
            let dataObject;
            let type = ComponentType.normalize(typeKey);
            switch (type) {
                case ComponentType.LAYOUT:
                    dataObject = nodeData;
                    break;
                case ComponentType.IMAGE:
                    dataObject = this.buildImage(nodeData);
                    break;
                case ComponentType.TITLE:
                    dataObject = this.buildTitle(nodeData);
                    break;
                case ComponentType.TEXT:
                    dataObject = this.buildText(nodeData);
                    break;
                case ComponentType.MARKUP:
                    dataObject = this.buildMarkup(nodeData);
                    break;
                default: Logger.warning('Unknown component type "' + type + '"');
            }
            return dataObject;
        }

        buildImage(data){
            this.imageBuilder.init();
            if (data.dimensions) {
                this.imageBuilder.withSourceAndSize(data.url, data.dimensions.width, data.dimensions.height);
            }
            if (data.mobile && data.mobile.dimensions) {
                this.imageBuilder.withSourceAndSize(data.mobile.url, data.mobile.dimensions.width, data.mobile.dimensions.height);
            }
            if (data.tablet && data.tablet.dimensions) {
                this.imageBuilder.withSourceAndSize(data.tablet.url, data.tablet.dimensions.width, data.tablet.dimensions.height);
            }
            if (data.desktop && data.desktop.dimensions) {
                this.imageBuilder.withSourceAndSize(data.desktop.url, data.desktop.dimensions.width, data.desktop.dimensions.height);
            }
            return this.imageBuilder.build();
        }

        buildText(data){
            this.textBuilder.init();
            for(let key in data) {
                let value = data[key];
                if ('paragraph' == value.type) {
                    this.textBuilder.withParagraph(value.text);
                }
            }
            return this.textBuilder.build();
        }

        buildMarkup(data){
            this.markupBuilder.init();
            for(let key in data) {
                let value = data[key];
                if ('paragraph' == value.type) {
                    this.markupBuilder.withParagraph(value.text);
                }
            }
            return this.markupBuilder.build();
        }

        buildTitle(data){
            let titleData = data.pop();
            this.titleBuilder.init();
            switch(titleData.type){
                case 'heading1':
                    this.titleBuilder.withHeader1(titleData.text);
                    break;
                case 'heading2':
                    this.titleBuilder.withHeader2(titleData.text);
                    break;
                case 'heading3':
                    this.titleBuilder.withHeader3(titleData.text);
                    break;
                case 'heading4':
                    this.titleBuilder.withHeader4(titleData.text);
                    break;
            }
            return this.titleBuilder.build();
        }
    }

    class HtmlTagBuilder{

        constructor(){
            this.init();
        }

        init(){
            this.tag = '';
            this.id = '';
            this.classes = [];
            this.isSelfClosing = false;
            this.content = '';
            this.attributes = {};
            return this;
        }

        withTag(tag){
            this.tag = tag;
            return this;
        }

        withId(id){
            this.id = id;
            return this;
        }

        withClasses(classes){
            this.classes = classes;
            return this;
        }

        addClass(cssClass){
            this.classes.push(cssClass);
            return this;
        }

        withIsSelfClosing(isSelfClosing){
            this.isSelfClosing = isSelfClosing;
            return this;
        }

        withContent(content){
            this.content = content;
            return this;
        }

        withAttributes(attributes){
            this.attributes = attributes;
            return this;
        }

        withAttribute(key, val){
            this.attributes[key] = val;
            return this;
        }

        /**
         *
         * @param {Element}element
         */
        withElement(element){
            this.tag = element.tag;
            this.id = element.id;
            this.classes = element.classes;
            this.isSelfClosing = element.selfClosing;
            this.content = element.content;
            this.attributes = element.attributes;
        }

        build(){

            if(!this.tag || '' == this.tag){
                throw 'Cannot create tag without tag name';
            }
            //let element = document.createDocumentFragment();
            //element.tagName = this.tag;
            let element = document.createElement(this.tag);


            // CLASSES
            this.classes.forEach(function(cssClass){
                element.classList.add(cssClass);
            });

            // ID
            if(!this._isEmptyString(this.id)){
                element.id = this.id;
            }

            // ATTRIBUTES
            for(let k in this.attributes){
                element.setAttribute(k, this.attributes[k]);
            }

            // CONTENT
            if(!this.isSelfClosing) {
                if (typeof this.content === 'string' || this.content instanceof String) {
                    if (!this._isEmptyString(this.content)) {
                        let textNode = document.createTextNode(this.content);
                        element.appendChild(textNode);
                    }
                } else if (typeof this.content === 'Element') {
                    let innerBuilder = new HtmlTagBuilder();
                    innerBuilder.init().withElement(this.content);
                    element.appendChild(innerBuilder.build());
                }
            }


            return element;

        }

        _isEmptyString(val){
            return !val || '' == val;
        }
    }

    class HtmlRenderer {

        constructor(){
            this.htmlTagBuilder = new HtmlTagBuilder();
        }

        render(layouts){
            let output = [];
            let builder = this.htmlTagBuilder;
            let layout;
            let layoutTag;
            let container;
            let containerTag;
            let component;
            let componentTag;
            let tag;
            let containerColumns = 12;
            let componentColumns = 12;
            let containerWidth;
            let componentWidth;
            for(let i = 0; i < layouts.length; i++){
                layout = layouts[i];
                layoutTag = builder.init()
                    .withTag('div')
                    .addClass('layout')
                    .addClass('layout-' + i)
                    .build();
                containerWidth = containerColumns / layout.containers.length;
                for(let j = 0; j < layout.containers.length; j++) {
                    container = layout.containers[j];
                    containerTag = builder.init()
                        .withTag('div')
                        .addClass('container')
                        .addClass('container-' + j)
                        .addClass('cols-' + containerWidth)
                        .build();
                    componentWidth = componentColumns / container.components.length;
                    for(let k = 0; k < container.components.length; k++){
                        component = container.components[k];
                        componentTag = builder.init()
                            .withTag('div')
                            .addClass('component')
                            .addClass('component-' + component.type)
                            .addClass('component-' + component.key)
                            .addClass('component-' + k)
                            .addClass('cols-' + componentColumns)
                            .build();
                        containerTag.appendChild(componentTag);
                        Logger.info('Tag: ' + component.data.tag);
                        builder.init().withElement(component.data);
                        tag = builder.build();
                        componentTag.appendChild(tag);
                        containerTag.appendChild(componentTag);
                    }
                    layoutTag.appendChild(containerTag);
                }
                output.push(layoutTag);
            }        return output;
        }


    }

    class Ichabod{

        constructor(contextOptions){
            let deaultOptions = {
                prismic: {
                    reference:'',
                    baseUrl:'',
                    searchPath:'/documents/search'
                },
                language:'',
                version:''
            };
            this.options = Object.assign(deaultOptions, contextOptions);
            this.facade = this._getFacade(this.options);        // get data
            this.adapter = this._getAdapter(this.options);      // convert to layouts
            this.renderer = this._getRenderer(this.options);    // render layouts to nodes
        }

        injectContent(searchContext, targetSelector){
            let searchOptions = Object.assign(searchContext, this.options);
            let that = this;
            this.facade.getData(searchOptions)
                // RENDER DATA
                .then(function(rawData){
                    let layouts = that.adapter.adapt(rawData);
                    Logger.info(layouts);
                    return new Promise(function (resolve, reject) {
                        if(layouts || 0 == layouts.results.length) {
                            resolve(layouts);
                        } else {
                            reject('Oh no! No rendered layouts to inject.');
                        }
                    });
                })
                // INJECT DATA
                .then(function(layouts){
                    //let renderer = new TextRenderer();
                    let nodes = that.renderer.render(layouts);
                    Logger.info('Getting Target "' + targetSelector + '"');
                    let parent = document.getElementById(targetSelector);
                    if(!parent){
                        Logger.warning('Element "' + targetSelector +'" not found.');
                    }                nodes.forEach(function(node){
                        parent.appendChild(node);
                    });
                });
        }

        injectLayouts(data){
            let target = data.target;
            let layouts = data.layout;
            //let renderer = new TextRenderer();
            let nodes = this.renderer.render(layouts);
            let parent = document.getElementById(target);
            nodes.forEach(function(node){
                parent.appendChild(node);
            });
        }

        _getFacade(options){
            let facade = null;
            if(options.prismic){
                if(Variable.isEmpty(options.prismic.baseUrl)){
                    throw 'When using the Prismic module, the baseUrl variable is required.';
                } else if(Variable.isEmpty(options.prismic.ref)){
                    throw 'When using the Prismic module, the ref variable is required.';
                }
                facade = new PrismicFacade(options.prismic.baseUrl);
            }
            return facade;
        }

        _getAdapter(options){
            let adapter = null;
            if(options.prismic){
                adapter = new PrismicAdapter();
            }
            return adapter;
        }

        _getRenderer(options){
            return new HtmlRenderer();
        }
    }

    return Ichabod;

}());
