#!/bin/bash

startingPoint='classes/Ichabod.js'
output='dist/ichabod.bundle.js'
# which can be one of "amd", "cjs", "system", "esm", "iife" or "umd"
format='iife'
rollup $startingPoint --file $output --format $format --name Ichabod
